package cn.itlaobing.vo;

import java.io.Serializable;

public class JsonResult<T> implements Serializable {
    //返回状态
    private Status status=Status.INFO;
    //返回消息
    private String message="";
    //返回的内容
    private T content;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public JsonResult(Status status, String message, T content) {
        this.status = status;
        this.message = message;
        this.content = content;
    }

    public JsonResult(Status status, String message) {
        this.status = status;
        this.message = message;
    }
}
