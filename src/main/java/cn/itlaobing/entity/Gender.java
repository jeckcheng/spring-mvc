package cn.itlaobing.entity;

public enum  Gender {
    FEMALE("女"),MALE("男");

    private String text;
    private Gender(String text){
        this.text=text;
    }

    public String getText() {
        return text;
    }
}
