package cn.itlaobing.web.interceptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
    Log log = LogFactory.getLog(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session=request.getSession();
       if(session.getAttribute("current_user")==null){  //从session中取出登录的用户如果为空，则表示没有登录
           //记录这次要访问的路径，等登录后再跳转到这个地址上
           session.setAttribute("back_url",request.getRequestURI());
           response.sendRedirect(request.getContextPath()+"/signin");//跳转到登录页面。
           return false;
       }
        return true;
    }
}
