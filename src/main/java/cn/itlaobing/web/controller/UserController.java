package cn.itlaobing.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/users")
public class UserController {

    Log log= LogFactory.getLog(this.getClass());

    @GetMapping
    public String index(Model model){ //查询所有用户
        //TODO
        return "";
    }
    @PostMapping
    public String create(){  //新增用户
        //TODO
        return "";
    }
    @GetMapping(path="/{id}")
    //{id}叫做路径参数PathVariable  ,在处理器方法上使用@PathVariable来获取到
    //路径参数的值，如果路径参数的名称和 处理器方法参数的名称不一致，就需要在@PathVariable指明
    //如果一致，则不需要指定路径参数的名称
    public String show(@PathVariable Long id){ //显示详情
        log.info(">>>>>>>>>>>show(@PathVariable Long id)>>>>>>>>"+id);
        return "";
    }
    @PutMapping(path="/{id}")
    public String update(@PathVariable Long id){ //修改用户
        log.info(">>>>>>>>>>>update(@PathVariable Long id)>>>>>>>>"+id);
        return "";
    }

    @DeleteMapping(path="/{id}")
    public String destroy(@PathVariable Long id){ //修改用户
        return "";
    }
}
