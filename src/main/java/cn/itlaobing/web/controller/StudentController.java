package cn.itlaobing.web.controller;

import cn.itlaobing.entity.Gender;
import cn.itlaobing.entity.Student;
import cn.itlaobing.service.StudentService;
import cn.itlaobing.vo.JsonResult;
import cn.itlaobing.vo.Status;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path = "/students")
public class StudentController {

    Log log= LogFactory.getLog(this.getClass());

    @Autowired
    private StudentService studentService;

    @GetMapping()
    public String index(Model model){
        log.info(">>>>>>>>>>>>>>index>>>>>>>>>>>>>>>>>>>");
        model.addAttribute("students",studentService.findAll());
        return "/student/index";
    }
    @GetMapping(path = "/create")
    public String create(){

        return "/student/create";
    }

    @PostMapping(path = "/create")
    public String create(@Valid @ModelAttribute Student student,
                         BindingResult bindingResult,
                         RedirectAttributes redirectAttributes){
        //执行处理器方法之前，首先要进行数据校验，会有一个结果 bindingResult
        if(bindingResult.hasErrors()){ //如果有错误，则应该返回到表单视图
            //因为表单需要与student对象绑定，所以它必须要放到Model中
            return "/student/create";
        }

        studentService.saveOrUpdate(student);
        //提示信息
        redirectAttributes.addFlashAttribute("message","添加成功");
        return "redirect:/students";
    }

    @GetMapping(path = "/{id}/update")
    public String update(){
        //因为prepareData 中已经获取到了数据，所以这里不用做任何操作
        return "/student/update";
    }



    @DeleteMapping(path = "/{id}")
    @ResponseBody
    public JsonResult<String> destroy(@PathVariable Long id){
        studentService.delete(id);
        JsonResult<String> jsonResult=new JsonResult<>(Status.SUCCESS,"删除成功");
        return jsonResult;
    }



    @ModelAttribute
    public void prepareData(HttpServletRequest request,Model model,@PathVariable(required = false) Long id){
        model.addAttribute("genders", Gender.values());
        model.addAttribute("ctx",request.getContextPath());
        if(id!=null){
            //如果请求路径中有id，则根据id将对象查询出来，然后放入到Model中
            model.addAttribute("student",studentService.findById(id));
        }else{

            model.addAttribute("student",new Student());
        }
    }

    @PutMapping(path = "/{id}/update")
    public String update(@ModelAttribute("student") Student student,RedirectAttributes redirectAttributes){
        studentService.saveOrUpdate(student);
        redirectAttributes.addFlashAttribute("message","修改成功");
        return "redirect:/students";
    }


}
