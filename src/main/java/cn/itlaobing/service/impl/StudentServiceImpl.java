package cn.itlaobing.service.impl;

import cn.itlaobing.entity.Gender;
import cn.itlaobing.entity.Student;
import cn.itlaobing.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private List<Student> students=new ArrayList<>();

    public StudentServiceImpl() {
        for(int i=1;i<=10;i++){
            Student student=new Student();
            student.setId(Long.valueOf(i));
            student.setBirthday(new Date());
            student.setGender(Gender.FEMALE);
            student.setName("student"+i);
            students.add(student);
        }
    }

    @Override
    public List<Student> findAll() {
        return students;
    }

    @Override
    public Student findById(Long id) {
        for(Student student:students){
            if(student.getId()==id){
                return student;
            }
        }
        return null;
    }

    @Override
    public Student saveOrUpdate(Student student) {
        if(student.getId()!=null){
            Student stu=findById(student.getId());
            if(stu!=null){
                stu.setName(student.getName());
                stu.setGender(student.getGender());
                stu.setBirthday(student.getBirthday());
            }
        }else{
            student.setId(Long.valueOf(students.size()*100+1));
            students.add(student);
        }
        return student;
    }

    @Override
    public void delete(Long id) {
        Student student=findById(id);
        students.remove(student);
    }
}
